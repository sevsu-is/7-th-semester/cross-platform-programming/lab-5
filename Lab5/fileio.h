#pragma once
#include <QObject>

class FileIO : public QObject
{
    Q_OBJECT
public:
    explicit FileIO(QObject *parent = nullptr);
    Q_INVOKABLE int exportTextToFile(QString text, QString filename);
    Q_INVOKABLE QString importTextFromFile(QString filename);
};
