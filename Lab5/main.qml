import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3

Window {
    width: 500
    height: 300
    visible: true
    title: qsTr("Text comparator")

    function textEditChanged() {
        compareLabel.reset()
    }

    function doComparison() {
        if (textEdit1.getText() === textEdit2.getText()) {
            compareLabel.matches()
        } else {
            compareLabel.doesNotMatch()
        }
    }

    function importFromFile(filename, textEditField) {
        const newText = fileio.importTextFromFile(filename)

        if (newText !== "") {
            textEditField.setText(newText)
            ioLabel.success();
        } else {
            ioLabel.error()
        }
    }

    function exportToFile(filename, textEditField) {
        let result = fileio.exportTextToFile(textEditField.getText(), filename)
        if (result !== 0) {
            ioLabel.error()
        } else {
            ioLabel.success();
        }
    }

    ColumnLayout {
        spacing: 5
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            CompareTextEdit {
                id: textEdit1
                defaultText: "Lorem ipsum"
                onCompareTextChanged: textEditChanged()
            }
            ColumnLayout {
                Button {
                    height: 35
                    label: "Import"
                    onButtonClicked: importFromFile("field1.txt", textEdit1)
                }
                Button {
                    height: 35
                    label: "Export"
                    onButtonClicked: exportToFile("field1.txt", textEdit1)
                }
            }
        }

        RowLayout {
            CompareTextEdit {
                id: textEdit2
                defaultText: "Lorem ipsum"
                onCompareTextChanged: textEditChanged()
            }
            ColumnLayout {
                Button {
                    height: 35
                    label: "Import"
                    onButtonClicked: importFromFile("field2.txt", textEdit2)
                }
                Button {
                    height: 35
                    label: "Export"
                    onButtonClicked: exportToFile("field2.txt", textEdit2)
                }
            }
        }

        RowLayout {
            spacing: 10
            Button {
                label: "Compare"
                onButtonClicked: doComparison()
            }

            ColumnLayout {
                spacing: 0
                CompareLabel {
                    id: compareLabel
                }
                FileIOStatusLabel {
                    id: ioLabel
                }
            }
        }
    }
}
