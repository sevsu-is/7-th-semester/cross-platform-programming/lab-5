#include "fileio.h"
#include <QTextStream>
#include <QFile>

FileIO::FileIO(QObject *parent)
    : QObject{parent}
{

}

bool fileOpenedForWriting(QFile& file) {
    return file.open(QIODevice::WriteOnly | QIODevice::Text);
}

int FileIO::exportTextToFile(QString text, QString filename) {
    QFile file(filename);

    if (fileOpenedForWriting(file)) {
        QTextStream exportStream(&file);
        exportStream << text;
        file.close();
        return 0;
    }
    return 1;
}

bool fileOpenedForReading(QFile& file) {
    return file.open(QIODevice::ReadOnly);
}

QString FileIO::importTextFromFile(QString filename) {
    QString text = "";
    QFile file(filename);

    if (fileOpenedForReading(file)) {
        QTextStream importStream(&file);

        while (!importStream.atEnd()) {
            text += importStream.readLine() + "\n";
        }

        file.close();
    }

    return text;
}
