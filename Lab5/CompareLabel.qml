import QtQuick 2.12

Rectangle {
    height: 50
    width: 100

    property string defaultLabel: "Result: Unknown. Press the button!"
    property string label: defaultLabel

    function reset() {
        label = defaultLabel
        resultLabel.color = "#3498db"
    }

    function matches() {
        label = "Result: A MATCH"
        resultLabel.color = "#229954"
    }

    function doesNotMatch() {
        label = "Result: NO MATCH"
        resultLabel.color = "#ff0000"
    }

    Text {
        id: resultLabel
        text: label
    }
}
