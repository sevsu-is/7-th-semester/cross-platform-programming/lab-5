import QtQuick 2.12

Rectangle {
    height: 50
    width: 100

    property string defaultLabel: "File I/O: No I/O activity"
    property string label: defaultLabel

    function error() {
        label = "File I/O: Operation failed!"
        resultLabel.color = "#ff0000"
    }

    function success() {
        label = "File I/O: Operation was successful!"
        resultLabel.color = "#229954"
    }

    Text {
        id: resultLabel
        text: label
    }
}
